<?php include __DIR__."/../components/header.php"; ?>
<?php 
$dadosImagem = $_FILES;
?>
<!-- Main Content -->
  <main class="content">
    <h1 class="title new-item">Novo Produto</h1>
    
    <form action="/salvaproduto"   method="POST" enctype="multipart/form-data">
      <div class="input-field">
        <label for="sku" class="label">Produto SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="nome" class="label">Produto Nome</label>
        <input type="text" id="name" name="nome" class="input-text" /> 
      </div>
        <div class="input-field">
        <label for="imagem" class="label">Foto Produto</label>
        <input type="file" accept="image/*" id="imagem" name="imagem" class="input-text"  /> 
      </div>
      <div class="input-field">
        <label for="preco" class="label">Preço</label>
        <input type="text" id="price" name="preco" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="quantidade" class="label">Quantidade</label>
        <input type="text" id="quantity" name="quantidade" class="input-text" /> 
      </div>
      <div class="input-field">
        <label for="category" class="label">Categorias</label>
        <select multiple name="categorias" id="category" class="input-text">
            <option value="categoria1">Category 1</option>
          <option>Category 2</option>
          <option>Category 3</option>
          <option>Category 4</option>
        </select>
      </div>
      <div class="input-field">
        <label for="descricao" class="label">Descrição</label>
        <textarea id="description" name="descricao" class="input-text"></textarea>
      </div>
      <div class="actions-form"> 
        <a href="/produtos" class="action back">Voltar</a>
        <input class="btn-submit btn-action" type="submit" name="btn_salvar" value="Salvar Produto" />
      </div>
      
    </form>
  </main>
  <!-- Main Content -->
<?php include __DIR__."/../components/footer.php"; ?>